#!/bin/sh

set -e
set -u

# init
version='0.0.21.1'
scriptname=$(basename $0)
mode=""
build_cmd=""
install_cmd=""
clean_cmd=""
src_dir=""
mex_dir=""
m_dir=""
makefile_mode=0

print_description()
{
cat << EOT
The is a small helper that eases building and installing MEX extensions for
Matlab toolbox packages in Debian binary packages. Because these packages
cannot build-depend on Matlab (for obvious reasons) they need to compile their
extensions at installation time using a local Matlab installation. The helper
is somewhat flexible by supporting custom build, install and clean commands, as
well as source and destination directories.  It also deals with moving
extensions into library directories and automatically symlinks them into the
toolbox directory.

There are two major modes: 'install' to build, install and symlink extensions
(useful in postinst) and 'clean' to remove installed extensions and symlinks
(useful in prerm).

The command to build the extensions is invoked in the source directory. By
default, this is /usr/src/matlab/<package name>, but can be overridden with
the --src-dir option. Any optional 'install' (--install-cmd) and 'clean'
(--clean-cmd) are invoked in the source directory too.

Moreover, this helper will also take any installed extensions from a default
installation path /usr/share/matlab/site/m/<package name>, move them into
/usr/lib/matlab/site/<package name> and symlink back to the original location.
These locations can be configured with the --m-dir and --mex-dir options
respectively. Again, this step is optional and is only performed if a package
actually installs extensions inot this location.

EOT
}


print_version()
{
cat << EOT
$scriptname $version

Copyright (C) 2010-2011 Michael Hanke <michael.hanke@gmail.com>

Licensed under GNU General Public License version 3 or later.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Written by Michael Hanke.
EOT
}

print_help()
{
cat << EOT
Usage:  $scriptname [OPTIONS] <package name> <mode>

Options:
-h
  Print usage summary and option list.

--help
  Print full help.

--version
  Print version information and exit.

--build-cmd
  Command to build the extensions in the source directory

--install-cmd
  Command to install the extensions after building

--clean-cmd
  Command to clean the source tree after installation. This is not the command
  that is executed in 'clean' mode.

--src-dir
  Directory with the extension sources. This is also the directory in which
  build, install and clean commands get invoked.

--mex-dir
  Target directory into which binary extensions get moved.

--m-dir
  Target directory in which symlinks to binary extensions get created.

--make
  Set default commands for 'build-cmd' (make), 'install-cmd'
  (make install DESTDIR=\$m_dir) and 'clean-cmd' (make distclean) if no
  specific command has been provided via the respective options.

EOT
}

print_examples()
{
cat << EOT
Examples:

The following call can be used in a package's postinst script if it comes with
a Matlab script 'build_matlab.m' that builds and installs its extension into
the desired locations. The --src-dir option is used to point to a non-standard
location of the extension sources.

  debian-matlab-mexhelper somepackagename install \
    --src-dir /usr/src/dynare-matlab/mex/sources \
    --build-cmd 'matlab -nodesktop -nodisplay -nojvm -r build_matlab'

If a package installs extension sources into the standard location and builds
its extensions using a Makefile that support the DESTDIR for installing the
built extensions and a 'distclean' target it is sufficient to run the
following.

  debian-matlab-mexhelper somepackagename install --make

Otherwise it is also possible to fully customize all commands.

  debian-matlab-mexhelper difficultpackage install \
    --build-cmd 'make -C src all toolbox MEXBIN="matlab-mex"' \
    --install-cmd 'make -C src install && find . ! -wholename "./src" -name "*.mex?*" -print0 | xargs -0 -I {} cp -v --parent {} /usr/share/difficultpackage' \
    --clean-cmd 'make -C src distclean toolbox-distclean && find . -name "*.mex?*" -delete'

If a package uses $scriptname to install extensions into the standard location
it can also be used to remove all MEX extensions and created symlinks when a
package is removed from a system. To achieve this simply put the following call
into a package's prerm script.

  debian-matlab-mexhelper packagename clean

EOT
}


# Parse commandline options (taken from the getopt examples from the Debian util-linux package)
# Note that we use `"$@"' to let each command-line parameter expand to a
# separate word. The quotes around `$@' are essential!
# We need CLOPTS as the `eval set --' would nuke the return value of getopt.
CLOPTS=`getopt -o h --long help,version,build-cmd:,install-cmd:,clean-cmd:,src-dir:,mex-dir:,m-dir: -n "$scriptname" -- "$@"`

if [ $? != 0 ] ; then
  echo "Terminating..." >&2
  exit 1
fi

# Note the quotes around `$CLOPTS': they are essential!
eval set -- "$CLOPTS"

while true ; do
  case "$1" in
    -h) print_help; exit 0;;
    --help) print_description; print_help; print_examples; exit 0;;
    --version) print_version; exit 0;;
    --build-cmd) shift; build_cmd=$1; shift;;
    --install-cmd) shift; install_cmd=$1; shift;;
    --clean-cmd) shift; clean_cmd=$1; shift;;
    --src-dir) shift; src_dir=$1; shift;;
    --mex-dir) shift; mex_dir=$1; shift;;
    --m-dir) shift; m_dir=$1; shift;;
    --make) shift; makefile_mode=1;;
    --) shift ; break ;;
    *) echo "Internal error! ($1)"; exit 1;;
  esac
done

if [ ! $# -eq 2 ]; then
  printf "Need exactly two arguments.\n\n"
  print_help
  exit 1
fi

pkg=$1
mode=$2

if [ -z "$pkg" ]; then
  echo "No package name given."
  print_help
  exit 1
fi

if [ -z "$mode" ]; then
  echo "No mode specified -- can be either 'install' or 'clean'."
  print_help
  exit 1
fi

# we have a default idea of where the sources are
if [ -z "$src_dir" ]; then
  src_dir="/usr/src/matlab/$pkg"
fi

# we have a default idea of where compile extensions shall live
if [ -z "$mex_dir" ]; then
  mex_dir="/usr/lib/matlab/site/$pkg"
fi

# we have a default idea of where extensions shall be symlinked to
if [ -z "$m_dir" ]; then
  m_dir="/usr/share/matlab/site/m/$pkg"
fi

# if in makefile mode set defaults
if [ $makefile_mode -eq 1 ]; then
    if [ -z "$build_cmd" ]; then
      build_cmd="make"
    fi
    if [ -z "$install_cmd" ]; then
      install_cmd="make install DESTDIR=$m_dir"
    fi
    if [ -z "$clean_cmd" ]; then
      clean_cmd="make distclean"
    fi
fi

# handle clean mode first
if [ "$mode" = "clean" ]; then
  # find MEX extension symlinks and remove
  [ -d $m_dir ] && find $m_dir/ -type l -name '*.mex?*' -delete
  # wipe out mexdir
  [ -d $mex_dir ] && rm -rf $mex_dir
  # done
  exit 0
fi

# unknown mode
if [ ! "$mode" = 'install' ]; then
  echo "Got unknown mode: $mode"
  exit 1
fi

# get the config
. /etc/matlab/debconf

# create logfile
logfile=$(mktemp ${TMPDIR:-/tmp}/${pkg}-mexbuild-$(date -u +%s).XXXXXXX)
cat << EOT >> $logfile
$(print_version)

Attempt to build Matlab extensions

Package name:        $pkg
Source directory:    $src_dir
MEX dir:             $mex_dir
M dir:               $m_dir
Build user (if any): $MATLAB_MEXBUILD_USER
Build command:       $build_cmd
Install command:     $install_cmd
Clean command:       $clean_cmd

EOT

echo "Attempt to build Matlab extensions:" >> $logfile

# go into the source dir
cd $src_dir

echo "Building Matlab extensions (logfile at $logfile)"

# if we need to use a dedicated user
# (strip whitespace from the user name -- necessary to avoid failure
# if the user name is basically empty but not length zero -- seems to happen
# when retrieving the value from debconf under some weired non-reproducible
# conditions)
if [ -n "$(echo ${MATLAB_MEXBUILD_USER} | tr -d ' ')" ]; then
  chown -R $MATLAB_MEXBUILD_USER $src_dir
  echo "sudo -H -u $MATLAB_MEXBUILD_USER sh -c \"$build_cmd\"" >> $logfile
  sudo -H -u $MATLAB_MEXBUILD_USER sh -c "$build_cmd" 2>&1 >> $logfile
  chown -R root:root $src_dir
else
  echo "$build_cmd" >> $logfile
  sh -c "$build_cmd" 2>&1 >> $logfile
fi

# now install if desired
if [ -n "$install_cmd" ]; then
  echo "$install_cmd" >> $logfile
  sh -c "$install_cmd" 2>&1 >> $logfile
fi

# now clean if desired
if [ -n "$clean_cmd" ]; then
  echo "$clean_cmd" >> $logfile
  sh -c "$clean_cmd" 2>&1 >> $logfile
fi

# look if installing created the m-path directory -- if so
# move binary extensions out of the m-path and symlink back
# However, if nothing got installed there we do not want to fail
# in case a specific setup is required or intended
[ ! -d $m_dir ] && exit 0

for ext in $(find $m_dir/ -type f -name '*.mex?*'); do
  # relative path to m-dir
  relpath=${ext#$m_dir/*}
  # extension location within the m-dir tree
  reldir=$(dirname $relpath)
  # create path in mex-dir
  mkdir -p $mex_dir/$reldir
  # move extension
  echo "mv $ext $mex_dir/$relpath" >> $logfile
  mv $ext $mex_dir/$relpath
  # might want to have nice function to figure out common prefix and to relative
  # symlinks
  echo "ln -s $mex_dir/$relpath $ext" >> $logfile
  ln -s $mex_dir/$relpath $ext
done

exit 0
