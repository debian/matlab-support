# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: matlab-support/title
Type: title
_Description: MATLAB interface configuration


Template: matlab-support/matlab-install-glob
Type: string
_Description: Location of MATLAB installation(s):
 The MATLAB interface needs to know where MATLAB is installed on this
 system. This can be specified as a single directory or, in case of multiple
 MATLAB installations, as a glob expression (any expression supported by bash,
 including extended pattern matching operators).
 .
 If, for example, the MATLAB executable is installed in /opt/matlab76/bin/matlab,
 please enter "/opt/matlab76". If there are multiple MATLAB versions
 installed, you can enter "/opt/matlab*" or a similar expression. Only
 matches that really contain a MATLAB executable will be considered.
 Therefore, a glob expression may match more than
 just MATLAB installation directories without negative side effects.


Template: matlab-support/default-version
Type: select
Choices: ${choices}
_Description: Default MATLAB version:
 The following MATLAB versions were found on this system. Please select which
 one should serve as the default MATLAB on this system.


Template: matlab-support/no-matlab-found
Type: error
_Description: No MATLAB installation found
 No MATLAB executables were found in the directories you specified.
 .
 This package requires at least one local installation of MATLAB.


Template: matlab-support/mexbuild-user
Type: string
_Description: Authorized user for MATLAB:
 If MATLAB can only be launched by a limited set of user accounts, please
 specify one of these. This account will be used by other
 packages to build MEX extensions upon installation.
 .
 You may leave this field empty if any user account 
 (including root) is allowed to launch MATLAB.


Template: matlab-support/rename-libs
Type: boolean
Default: false
_Description: Rename MATLAB's GCC libraries?
 A MATLAB installation is shipped with copies of GCC dynamic loadable
 libraries, which typically come from an old version of GCC.
 .
 These libraries sometimes cause conflicts.
 .
 If you choose this option, the conflicting libraries will be renamed by appending
 a ".bak" extension. These libraries are located in the "sys/os/glnx86" or
 "sys/os/glnxa64" subdirectory of a MATLAB installation tree.
